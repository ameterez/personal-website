from django_cron import CronJobBase, Schedule
from django.db import transaction
import os
import requests
from .models import Project

class UpdateProjectsDB(CronJobBase):
    RUN_EVERY_MINS = 60
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = os.environ['CRON_CODE']
    
    def do(self):
        req = requests.get('https://api.github.com/users/alexandrumeterez/repos')
        print(req)
        body = req.json()
        new_ids = [b['id'] for b in body]
        old_ids = Project.objects.values_list('project_id', flat=True)
        
        pending_delete = list(set(old_ids) - set(new_ids))
        with transaction.atomic():
            for pending_id in pending_delete:
                Project.objects.filter(project_id=pending_id).delete()
            for entry in body:
                project_id = entry['id']
                project_name = entry['name']
                if project_name is None:
                    project_name = "No name"
                project_description = entry['description']
                if project_description is None:
                    project_description = "No description."
                used_language = entry['language']
                if used_language is None:
                    used_language = "No language"
                Project.objects.update_or_create(project_id=project_id, defaults={'project_name':project_name.capitalize(), \
                                                'project_description':project_description.capitalize(), 'used_language':used_language.capitalize()})