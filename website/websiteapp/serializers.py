from rest_framework import serializers
from websiteapp.models import Project 

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('project_name', 'project_description', 'used_language')