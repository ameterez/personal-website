from django.db import models

class Project(models.Model):
    project_id = models.IntegerField(primary_key=True, default=0)
    project_name = models.CharField(max_length=100, default="Project")
    project_description = models.CharField(max_length=400, default="Description")
    used_language = models.CharField(max_length=100, default="Language")

    class Meta:
        ordering = ('project_name',)
