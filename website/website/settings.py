import os
if 'DJANGO_SETTINGS' in os.environ:
    if os.environ['DJANGO_SETTINGS'] == 'dev':
        print("Dev Server")
        from .settings_dev import *
    else:
        print("Prod Server")
        from .settings_prod import *